﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hristoforov04
{
    class Program
    {
        static void Main(string[] args)
        {
            var studList = new Container();
            studList.Add(new Student("Луговой Александр Евгениевич", new DateTime(2000, 7, 2), new DateTime(2017, 9, 1), "Б", 117, "КИТ", "Компьютерные игры", 80));
            studList.Add(new Student("Христофоров Кирилл Андреевич", new DateTime(2002, 4, 8), new DateTime(2019, 9, 1), "Б", 119, "КИТ", "Компьютерные игры", 78));
            studList.Add(new Student("Мыглин Андрей Александрович", new DateTime(2002, 3, 31), new DateTime(2019, 9, 1), "Б", 119, "КИТ", "Компьютерные игры", 67));
            studList.Add(new Student("Усадьба Владимир Георгиевич", new DateTime(1999, 2, 11), new DateTime(2016, 9, 1), "В", 416, "КИТ", "АТМ", 40));
            studList.Add(new Student("Комендант Олег Олегович", new DateTime(2003, 5, 1), new DateTime(2019, 9, 1), "Б", 419, "КИТ", "АТМ", 100));
            var name = "Христофоров Кирилл Андреевич";
            var stud = studList.GetStudent(name);

            Console.WriteLine($"Группа студента {name}: {stud.Group}");
            Console.WriteLine("\n");

            Console.WriteLine($"Курс студента {name}: {stud.Year}");
            Console.WriteLine("\n");

            Console.WriteLine($"Семестр студента {name}: {stud.Semester}");
            Console.WriteLine("\n");

            Console.WriteLine($"Возраст студента {name}: {stud.Age}");
            Console.WriteLine("\n");
            Console.ReadLine();
        }
    }
}
