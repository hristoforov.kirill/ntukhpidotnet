﻿using System;
using System.Linq;

namespace hristoforov04
{
    // Класс Студенты
    public class Student
    {
        private readonly string name;
        private readonly DateTime dateOfBirth;
        private readonly DateTime dateOfAdmission;
        private string groupIndex;
        private int groupNum;
        private string faculty;
        private string specialty;
        private int performance;
        private string[] faculties = { "Э", "МИТ", "И", "ХТ", "БЭМ", "МО", "СГТ", "КН", "КИТ" };

        public Student(string name, DateTime dateOfBirth, DateTime dateOfAdmission, string groupIndex, int groupNum, string faculty, string specialty, int performance)
        {
            this.name = name ?? throw new ArgumentNullException(nameof(name));
            this.dateOfBirth = dateOfBirth;
            this.dateOfAdmission = dateOfAdmission;
            this.groupIndex = groupIndex ?? throw new ArgumentNullException(nameof(groupIndex));
            this.groupNum = groupNum;
            this.faculty = faculty ?? throw new ArgumentNullException(nameof(faculty));
            this.specialty = specialty ?? throw new ArgumentNullException(nameof(specialty));
            this.performance = performance;
        }

        public string Name { get { return name; } }
        public DateTime DateOfBith { get { return dateOfBirth; } }
        public DateTime DateOfAdmission { get { return dateOfAdmission; } }
        public string GroupIndex
        {
            get { return groupIndex; }
            private set
            {
                if (value.Length <= 2 && value.Length != 0)
                {
                    groupIndex = value;
                }
            }
        }
        public string Group
        {
            get
            {
                return $"{faculty}-{groupNum}{GroupIndex}";
            }
        }

        public int Year
        {
            get
            {
                int year = (int)((DateTime.Now - dateOfAdmission).TotalDays / 365.2425) + 1;
                if (year > 6)
                {
                    year = 6;
                }
                else if (year < 0)
                {
                    year = 0;
                }
                return year;
            }
        }

        public int Semester
        {
            get
            {
                int day = (int)((DateTime.Now - dateOfAdmission).TotalDays % 365.2425);
                if (day < 150)
                {
                    return 1;
                }
                else return 2;
            }
        }

        public int Age
        {
            get
            {
                return (int)((DateTime.Now - dateOfBirth).TotalDays / 365.2425);
            }
        }

        public int GroupNum { get { return groupNum; } private set { groupNum = value; } }
        public string Faculty
        {
            get { return faculty; }
            private set
            {
                if (faculties.Contains(value.ToUpper()))
                {
                    faculty = value.ToUpper();
                }
            }
        }
        public string Specialty { get { return specialty; } private set { specialty = value; } }
        public int Performance
        {
            get { return performance; }
            set
            {
                if (value <= 100 && value >= 0)
                {
                    performance = value;
                }
            }
        }
        public override string ToString()
        {
            string output = $"{name};{dateOfBirth.ToString()};{dateOfAdmission};{faculty};{groupNum};{groupIndex};{specialty};{performance}";
            return output;
        }
        public static Student ParseString(string line)
        {
            string[] arr = line.Split(';');
            Student stud = new Student(arr[0], DateTime.Parse(arr[1]), DateTime.Parse(arr[2]), arr[5], Int32.Parse(arr[4]), arr[3], arr[6], Int32.Parse(arr[7]));
            return stud;
        }
        public string ToText()
        {
            string output = $"Ф.И.О.: {name}\nДата рождения: {dateOfBirth.ToString()}\nДата поступления: {dateOfAdmission}\nГруппа: {faculty}-{groupNum}{groupIndex}\nСпециальность: {specialty}\nУспеваемость: {performance}%";
            return output;
        }
    }
}
