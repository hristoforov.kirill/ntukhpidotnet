﻿using System;

namespace hristoforov03
{
	// Класс Студенты
	public class Student
	{
		private string name;
		private DateTime dateOfBirthday;
		private int ageOfAdmission;
		private string groupInfo;
		private int progress;


		//Конструктор без параметров
		public Student() { }
		// Конструктор с параметрами 
		public Student(string n, DateTime birthDate,int admission, string group, int prog)
		{
			name = n;
			dateOfBirthday = birthDate;

			ageOfAdmission = admission;
			groupInfo = group;
			progress = prog;
		}

		public string Name { get { return name; } set { name = value; } }

		public DateTime DateOfBirthday { get { return dateOfBirthday; } set { dateOfBirthday = value; } }

		public int AgeOfAdmission { get { return ageOfAdmission; } set { ageOfAdmission = value; } }

		public string GroupInfo { get { return groupInfo; } set { groupInfo = value; } }

		public int Progress { get { return progress; } set { progress = value; } }

		// Переопределенный метод ToString()
		public override string ToString()
		{
			string output = $"{Name};{dateOfBirthday.ToShortDateString()};{ageOfAdmission};{groupInfo};{progress}";
			return output;
		}
		public string ToText()
		{
			return $"ФИО:{ Name }, Дата рождения: " + dateOfBirthday.ToShortDateString() + ", Год поступления: "
					+ ageOfAdmission + ", Группа: " + groupInfo + ", Прогресс: " + progress + "%";
		}
		public static Student ParseString(string line)
		{ 
			string[] arr = line.Split(';');
			Student stud = new Student(arr[0], DateTime.Parse(arr[1]), Int32.Parse(arr[2]), arr[3], Int32.Parse(arr[4]));
			return stud;
		}
	}
}
