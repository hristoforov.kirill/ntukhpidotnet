﻿
using System;

namespace hristoforov03
{
    public class Program
    {
        static void Main(string[] args)
        {
            var studList = new Container();
            studList.Add(new Student("Луговой Александр Евгениевич", new DateTime(2000, 7, 2), 2017, "KIT-117b", 80));
            studList.Add(new Student("Христофоров Кирилл Андреевич", new DateTime(2002, 4, 8), 2019, "KIT-119b", 78));
            studList.Add(new Student("Усадьба Владимир Георгиевич", new DateTime(1999, 2, 11), 2016, "KIT-116b", 40));
            studList.Add(new Student("Комендант Олег Олегович", new DateTime(2003, 5, 1), 2019, "KIT-119b", 100));
            ContainerFile workWithFile = new ContainerFile(studList);
            workWithFile.Write(null, "C:\\Users\\User\\source\\repos\\c#\\hristoforov03\\output.txt");
            var studList2 = workWithFile.Read("C:\\Users\\User\\source\\repos\\c#\\hristoforov03\\output.txt");
            var name = "Христофоров Кирилл Андреевич";
            var stud = studList2.GetStudent(name);
            stud.Progress += 10;
            studList2.ChangeStudent(name, stud);
            studList2.PrintAll(false);
            Console.ReadLine();
        }
    }
}
