﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace hristoforov03
{
	//Класс для проверки валидации данных
	public class Validation
	{
		public static bool checkFIO(string line)
		{
			string regex = @"[А-Я][а-я]*(\s[А-Я][A-я]+){2}";
			Regex r = new Regex(regex);

			Match m = r.Match(line);
			if (m.Success)
			{
				return true;
			}
			else
			{
				Console.WriteLine("Ввод не коректен, попробуйте снова!");
				return false;
			}
		}

		public static bool checkYear(int year)
		{
			string regex = @"^([1][8-9]\d{2}|[2]([0]\d{2}|[1][0]{2}))$";
			string syear = year.ToString();
			Regex r = new Regex(regex);

			Match m = r.Match(syear);
			if (m.Success)
			{
				return true;
			}
			else
			{
				Console.WriteLine("Ввод не коректен, попробуйте снова!");
				return false;
			}
		}

		public static bool checkMonth(int month)
		{
			string regex = @"^([1-9]|[1][0-2])$";
			string smonth = month.ToString();
			Regex r = new Regex(regex);

			Match m = r.Match(smonth);
			if (m.Success)
			{
				return true;
			}
			else
			{
				Console.WriteLine("Ввод не коректен, попробуйте снова!");
				return false;
			}
		}

		public static bool checkDay(int day)
		{
			string regex = @"^([1-9]|[1-2][0-9]|[3][0-1])$";
			string sday = day.ToString();
			Regex r = new Regex(regex);

			Match m = r.Match(sday);
			if (m.Success)
			{
				return true;
			}
			else
			{
				Console.WriteLine("Ввод не коректен, попробуйте снова!");
				return false;
			}
		}
	}
}
