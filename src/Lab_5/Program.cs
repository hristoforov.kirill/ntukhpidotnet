﻿using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace hristoforov05
{
    class Program
    {
        delegate int Average(Container records, int number, String str);
        static void Main(string[] args)
        {
            StringBuilder sb = new StringBuilder();
            var studArr = new Container();
            studArr.Add(new Student("Кирилл","Христофров","Андреевич", new DateTime(2001, 12, 8), new DateTime(2019, 9, 1),'Б',"КИТ",124,78));
            studArr.Add(new Student("Андрей","Мыглин","Александрович", new DateTime(2002, 3, 31), new DateTime(2019, 9, 1),'Б',"КИТ",123,67));
            studArr.Add(new Student("Владимир", "Зайцев", "Иванович", new DateTime(2002, 5, 21), new DateTime(2019, 9, 1),'А',"КИТ",123,94));
            studArr.Add(new Student("Александр","Луговой","Евгенивевич", new DateTime(2002, 4, 2), new DateTime(2019, 9, 1),'В',"КИТ",123,78));
          
            Console.WriteLine(studArr.Group(studArr, 3));
            Console.WriteLine("Вывести список студентов по группе:");
            studArr.Print(studArr, 1, "КИТ-2019Б");
            Console.WriteLine("Вывести список студентов по специальности:");
            studArr.Print(studArr, 2, "123");
            Console.WriteLine("Вывести список студентов по факультету:");
            studArr.Print(studArr, 3, "КИТ");

            studArr.GrRemove(studArr, 2, "124");

            Console.WriteLine("Средний возраст: " + studArr.AvAge(studArr, 2, "123"));
            Console.WriteLine("Средняя успеваемость: " + studArr.AvProgress(studArr, 3, "КИТ"));


            XmlSerializer formatter = new XmlSerializer(typeof(Student[]));

            using (FileStream fs = new FileStream("C:\\Users\\User\\source\\repos\\c#\\hristoforov05\\students.xml", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, studArr.students);
            }

            using (FileStream fs = new FileStream("C:\\Users\\User\\source\\repos\\c#\\hristoforov05\\students.xml", FileMode.OpenOrCreate))
            {
                Student[] newStud = (Student[])formatter.Deserialize(fs);

                foreach (Student p in newStud)
                {
                    Console.WriteLine(p);
                }
            }
            Console.ReadLine();
        }
    }
}
