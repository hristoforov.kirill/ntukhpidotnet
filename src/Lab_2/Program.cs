﻿
using System;

namespace Lab_2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Container studList = new Container();
            studList.Add(new Student("Луговой Александр Евгениевич", 2, 7, 2000, 2017, "KIT-117b", 80));
            studList.Add(new Student("Христофоров Кирилл Андреевич", 4, 8, 2002, 2019, "KIT-119b", 78));
            studList.Add(new Student("Усадьба Владимир Георгиевич", 11, 2, 1999, 2016, "KIT-116b", 40));
            studList.Add(new Student("Комендант Олег Олегович", 1, 5, 2003, 2019, "KIT-119b", 100));
            Console.WriteLine("\nВывод в полном формате\n");
            studList.PrintAll(false);
            Console.WriteLine("\nУдаление элемента на позиции 2 и вывод в кратком формате: \n");
            studList.Delete(2);
            studList.PrintAll(true);
            Console.WriteLine("\nПолучение информации о конкретном студенте по имени Комендант Олег Олегович: \n");
            Console.WriteLine(studList.GetStudent("Комендант Олег Олегович").ToString());
            int i = 1;
            Console.WriteLine("\nВывод студентов с их успеваемостью: \n");
            foreach (Student stud in studList)
            {
                Console.WriteLine(i + "." + stud.Name + ": " + stud.Progress + "\n");
                i++;
            }
            Console.ReadLine();
        }
    }
}
