﻿using System;
using System.Text.RegularExpressions;

namespace Lab_1
{
	//Класс для проверки валидации данных
	public class Validation
	{
		public static bool CheckFIO(string line)
		{
			string regex = @"[А-Я][а-я]*(\s[А-Я][A-я]+){2}";
			var r = new Regex(regex);

			var m = r.Match(line);
			if (m.Success)
			{
				return true;
			}
			else
			{
				Console.WriteLine("Ввод не коректен, попробуйте снова!");
				return false;
			}
		}

		public static bool CheckYear(int year)
		{
			string regex = @"^([1][8-9]\d{2}|[2]([0]\d{2}|[1][0]{2}))$";
			string syear = year.ToString();
			var r = new Regex(regex);

			var m = r.Match(syear);
			if (m.Success)
			{
				return true;
			}
			else
			{
				Console.WriteLine("Ввод не коректен, попробуйте снова!");
				return false;
			}
		}

		public static bool CheckMonth(int month)
		{
			string regex = @"^([1-9]|[1][0-2])$";
			string smonth = month.ToString();
			var r = new Regex(regex);

			var m = r.Match(smonth);
			if (m.Success)
			{
				return true;
			}
			else
			{
				Console.WriteLine("Ввод не коректен, попробуйте снова!");
				return false;
			}
		}

		public static bool CheckDay(int day)
		{
			string regex = @"^([1-9]|[1-2][0-9]|[3][0-1])$";
			string sday = day.ToString();
			var r = new Regex(regex);

			var m = r.Match(sday);
			if (m.Success)
			{
				return true;
			}
			else
			{
				Console.WriteLine("Ввод не коректен, попробуйте снова!");
				return false;
			}
		}
	}
}
