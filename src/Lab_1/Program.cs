﻿using System;
using System.Collections;

namespace Lab_1
{
    public class Program
    {
        static void Main(string[] args)
        {
            var list = new ArrayList();// Лучше лист
            string studentInfo = "";
            int yearOfBirth = 0;
            int monthOfBirth = 0;
            int dayOfBirth = 0;
            int ageOfAdmission = 0;
            string groupInfo = "";
            int progress = 0;
            while (true)
            {
                Console.WriteLine("1 - Добавление элемента. \n"
                            + "2 - Очистка списка.\n"
                            + "3 - Удаление элемента. \n"
                            + "4 - Вывод содержимого. \n"
                            + "5 - Выход. \n");
                Console.WriteLine("Выберете команду: \n");
                int number = int.Parse(Console.ReadLine());
                switch (number)
                {
                    case 1:
                        bool res = false;
                        while (res == false)
                        {
                            Console.WriteLine("ФИО: ");
                            studentInfo = Console.ReadLine();
                            if (Validation.CheckFIO(studentInfo))
                            {
                                res = true;
                            }
                        }
                        res = false;
                        while (res == false)
                        {
                            Console.WriteLine("Год рождения : ");
                            yearOfBirth = int.Parse(Console.ReadLine()); ;
                            if (Validation.CheckYear(yearOfBirth))
                            {
                                res = true;
                            }
                        }
                        res = false;
                        while (res == false)
                        {
                            Console.WriteLine("Месяц рождения: ");
                            monthOfBirth = int.Parse(Console.ReadLine());
                            if (Validation.CheckMonth(monthOfBirth))
                            {
                                res = true;
                            }
                        }
                        res = false;
                        while (res == false)
                        {
                            Console.WriteLine("День рождения: ");
                            dayOfBirth = int.Parse(Console.ReadLine());
                            if (Validation.CheckDay(dayOfBirth))
                            {
                                res = true;
                            }
                        }
                        res = false;
                        while (res == false)
                        {
                            Console.WriteLine("Год поступления: ");
                            ageOfAdmission = int.Parse(Console.ReadLine());
                            if (Validation.CheckYear(ageOfAdmission))
                            {
                                res = true;
                            }
                        }
                        res = false;
                        while (res == false)
                        {
                            Console.WriteLine("Группа");
                            groupInfo = Console.ReadLine();
                            res = true;

                        }
                        res = false;
                        while (res == false)
                        {
                            Console.WriteLine("Успеваемость: ");
                            progress = int.Parse(Console.ReadLine());
                            if (progress <= 100 && progress >= 0)
                            {
                                res = true;
                            }

                        }
                        list.Add(new Student(studentInfo, dayOfBirth, monthOfBirth, yearOfBirth, ageOfAdmission, groupInfo, progress));
                        break;
                    case 2:
                        list.Clear();
                        break;
                    case 3:
                        Console.WriteLine("Выберите позицию элемента: ");
                        int pos = int.Parse(Console.ReadLine());
                        list.RemoveAt(pos);
                        break;
                    case 4:
                        foreach (Student student in list)
                        {
                            Console.WriteLine(student.ToString());
                        }
                        break;
                    case 5:
                        return;
                }
            }
        }
    }
}
